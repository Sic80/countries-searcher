import { createStore } from 'redux';
import { themeToggle } from './components/reducers';
import storage from 'redux-persist/lib/storage';
import { persistReducer } from 'redux-persist';
import autoMergeLevel2 from 'redux-persist/lib/stateReconciler/autoMergeLevel2';



const persistConfig = {
    key: 'root',
    storage,
    stateReconciler: autoMergeLevel2,
} 

const persistedReducers = persistReducer(persistConfig, themeToggle);

export const configureStore = () => createStore(persistedReducers);