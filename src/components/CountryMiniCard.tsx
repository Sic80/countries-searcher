import { useHistory } from "react-router-dom";
import CountryInformation from "./CountryInformation";
import { Country } from "./types/CountryInterface";

export interface CountryMiniCardProps {
    isDarkMode: boolean;
    country: Country;
}
 
const CountryMiniCard: React.FunctionComponent<CountryMiniCardProps> = ({ isDarkMode, country}) => {

    const history = useHistory();
    const { name, region, capital, flags, population } = country;
    const popFormat = new Intl.NumberFormat('de-DE').format(population);

    return ( 
        <div className="grid grid-cols-10 justify-center relative z-0">
            <div  
                className={`col-span-8 col-start-2 flex flex-col justify-between ${isDarkMode ? "bg-myGray text-gray-200" : "bg-white"} shadow-md mb-10 rounded-lg cursor-pointer`}
                onClick={() => {
                    history.push(`${name.replaceAll(/(\(|\)|\s)/g  , "-")}`);
                    window.scrollTo(0, 0);
                }}
        >   
                <img className="w-full shadow-md rounded-t-lg`" src={`${flags.svg}`} alt={`${country} flag`}/>
                <div className="mt-8 h-36 ml-8 text-sm font-semibold">
                    <p className="font-bold mb-4 ">{name}</p>
                    <CountryInformation label="Population" value={popFormat} />
                    <CountryInformation label="Region" value={region} />
                    <CountryInformation label="Capital" value={capital} />
                </div>
            </div>
        </div>
     );
}
 
export default CountryMiniCard;