import { useParams } from 'react-router-dom';
import { Country } from './types/CountryInterface';
import Header from './Header';
import { useHistory } from "react-router-dom";
import { Currency } from "./types/CurrencyInterface";
import { Language } from "./types/LanguageInterface";
import { useSelector } from 'react-redux';
import { RootState } from './types/RootState';
import CountryInformation from './CountryInformation';
 
const CountryCard: React.FunctionComponent = () => {

    const { country } = useParams<{country: string}>();
    const jsonCountries = localStorage.getItem("countries");
    const countries = jsonCountries !== null ? JSON.parse(jsonCountries) : [];
    const currentCountry = countries.find((item: Country) => item.name.replaceAll(/(\(|\)|\s)/g  , "-") === country);
    const history = useHistory();
    const { name, flags, area, nativeName, region, subRegion, capital, topLevelDomain, population, currencies, languages, borders } = currentCountry || {};
    const popFormat = new Intl.NumberFormat('de-DE').format(population);
    const areaFormat = `${new Intl.NumberFormat('de-DE').format(area)} km²`;
    const isDarkMode = useSelector((state: RootState) => state.isDarkMode);
    const allLanguages = languages.map((language: Language) => language.name).join(", ");
    const allCurrencies = currencies.map((currency: Currency) => currency.name).join(", ");
    const neighbouringCountries = borders ? borders.map((border: string) => countries.find((country: Country) => country.alpha3Code === border).name) : null;

    return ( 
        <div className={`h-full min-h-screen ${isDarkMode ? "bg-gray-800 text-gray-200" : "bg-lightMode"}`}>
            <Header />
            <button 
                className={`mt-4 ml-6 border-2 w-20 shadow-md xl:w-32 xl:h-10 rounded-md xl:rounded-lg xl:ml-24 xl:mt-10 ${isDarkMode ? "bg-myGray border-gray-900" : "bg-white"}`}
                onClick={() => history.push('/')}
            >
                <span className="mr-4">←</span><span>Back</span>
            </button>
           {/*<button
                className={`mt-4 ml-6 border-2 w-20 shadow-md xl:w-32 xl:h-10 xl:rounded-lg xl:ml-24 xl:mt-10 ${isDarkMode ? "bg-myGray border-gray-900" : "bg-white"}`}
                onClick={() => history.push(`${countries[(countries.indexOf(currentCountry) - 1)].name.replaceAll(/(\(|\))/g  , "-")}`)}
            >
                Previous
            </button>
            <button
                className={`mt-4 ml-6 border-2 w-20 shadow-md xl:w-32 xl:h-10 xl:rounded-lg xl:ml-24 xl:mt-10 ${isDarkMode ? "bg-myGray border-gray-900" : "bg-white"}`}
                onClick={() => history.push(`${countries[(countries.indexOf(currentCountry) + 1)].name.replaceAll(/(\(|\))/g  , "-")}`)}
            >
                Next
           </button>*/}
            <div className="grid grid-cols-1 md:grid-cols-2  md:mt-0 xl:mt-0 self-center pr-2"> 
                    <div 
                        className={`grid h-auto md:h-80 ml-4 mr-4 xl:ml-20 xl:mr-20 mt-14 justify-center object-contain ${isDarkMode ? "bg-gray-800 border-gray-800" : "bg-lightMode"}`}   
                    >
                        <img 
                            className={` h-auto sm:h-auto xl:h-auto md:max-h-80  self-center shadow-lg`}  
                            src={`${flags.svg}`} 
                            alt={`${name} flag`}
                        />                        
                    </div>
                <div className="grid grid-cols-1 md:grid-cols-2 ml-6 xl:mt-10">
                    <div className="md:col-span-2">
                        <p className="mt-10  xl:mt-6 font-bold text-lg xl:text-2xl">{name}</p>
                    </div> 
                    <div className="mt-3 pt-3 space-y-3 font-semibold text-sm xl:text-sm">
                        <CountryInformation label="Native Name" value={nativeName} />
                        <CountryInformation label="Population" value={popFormat} />
                        <CountryInformation label="Region" value={region} />
                        <CountryInformation label="Sub Region" value={subRegion} />
                        <CountryInformation label="Capital" value={capital} />    
                    </div>
                    <div className="mt-3 xl:pt-3 space-y-3 font-semibold text-sm xl:text-sm">
                        <CountryInformation label="Area" value={areaFormat} />
                        <CountryInformation label="Top Level Domain" value={topLevelDomain} /> 
                        <CountryInformation label="Currencies" value={allCurrencies} /> 
                        <CountryInformation label="Languages" value={allLanguages} />
                    </div>
                    <div className="mt-10 grid grid-cols-1 md:col-span-2 xl:grid-cols-5">
                        <p className="font-semibold xl:text-sm">
                            Border Countries:
                        </p>
                        <div className="grid grid-cols-3 xl:grid-cols-4 mt-4 xl:mt-0 pb-10 mr-6 gap-2 xl:col-span-3">
                            {neighbouringCountries && neighbouringCountries.map((country: string, borderIdx: number) => 
                                <button 
                                    key={borderIdx} 
                                    className={` border-2 rounded-md shadow-sm text-sm h-auto pt-2 pb-2 xl:pt-0 xl:pb-0 ${isDarkMode ? "bg-myGray border-gray-800 text-gray-100 text-sm" : "bg-white"}`}
                                    onClick={(e) => {
                                        history.push(`${country.replaceAll(/(\(|\)|\s)/g  , "-")}`);
                                        console.log(country.replaceAll(/(\(|\)|\s)/g  , "-"))
                                        window.scrollTo(0, 0);
                                    }}
                                >
                                    <p className={`pt-2 pb-2 ${isDarkMode ? "text-gray-100 text-xs" : "text-gray-500"}`}>
                                        {country.replace(/ *\([^)]*\) */g, "")}
                                    </p>
                                </button>
                            )}
                        </div>
                    </div>
                </div>
            </div>
        </div>
       
    );
}
 
export default CountryCard;