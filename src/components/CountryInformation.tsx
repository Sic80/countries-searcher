export interface CountryInformationProps {
    label: string;
    value: string|number;
}
 
const CountryInformation: React.FunctionComponent<CountryInformationProps> = ({ label, value }) => {
    return ( 
        <p>{label}: <span className="font-normal text-gray-500">{value || '–'}</span></p>
     );
}
 
export default CountryInformation;