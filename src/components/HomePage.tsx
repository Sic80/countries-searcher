import "external-svg-loader";
import * as React from 'react';
import { useState, useEffect } from 'react';
import { Country } from './types/CountryInterface';
import Header from './Header';
import Dropdown from './Dropdown';
import { AiOutlineSearch } from 'react-icons/ai';
import { useSelector } from 'react-redux';
import { RootState } from './types/RootState';
import CountryMiniCard from "./CountryMiniCard";


export interface HomePageProps {
    countries: Country[];
}
 
const HomePage: React.FunctionComponent<HomePageProps> = ({ countries }) => {

    const _ = require('lodash');
    const [selectedRegion, setSelectedRegion] = useState<string>("Filter by Region (all)");
    const [inputValue, setInputValue] = useState<string>("");
    const [countriesToDisplay, setCountriesToDisplay] = useState<Country[]>(countries);
    const isDarkMode = useSelector((state: RootState) => state.isDarkMode);

    useEffect(() => {
        const countriesInRegion = countries.filter((country: Country) => country.region === selectedRegion);
        const countriesFilteredByRegion = countriesInRegion.length > 0 ? countriesInRegion : countries;
        const searchedCountries = countriesFilteredByRegion.filter((country: Country) => country.name.includes(inputValue));
        setCountriesToDisplay(searchedCountries);
    }, [selectedRegion, inputValue, countries]);

    return ( 
        <div className={` min-h-screen ${isDarkMode ? " bg-gray-800" : `bg-lightMode`}`}>
            <Header />
            <div className="grid grid-cols-1 md:grid-cols-2 md:mt-6 select-none">
                <div className={`h-14 xl:h-16 grid grid-cols-8 mt-5 ml-5 xl:ml-10 xl:w-1/2  mr-5 ${isDarkMode? "bg-gray-700 text-white" : "bg-white"} items-center rounded-md shadow-md mb-10`}>
                    <div className=" h-12 col-span-1 grid justify-items-center items-center pl-5">
                        <AiOutlineSearch size={20} color={isDarkMode ? "white" : `grey`}/> 
                    </div>
                    <input 
                        className={`h-12 col-span-7 pl-5 text-sm xl:text-lg outline-none ${isDarkMode && "bg-gray-700 text-white" }`}
                        placeholder="Search for a country..."
                        value={inputValue}
                        onChange={(e) => setInputValue(_.capitalize(e.target.value))}
                    />
                </div>
                <Dropdown 
                    selectedRegion={selectedRegion}
                    setSelectedRegion={setSelectedRegion}
                />
            </div>
            <div className="grid grid-cols-1 xl:grid-cols-4 md:mt-3 md:grid-cols-3 sm:grid-cols-2 sm:mt-5">
                
                {countriesToDisplay.length > 0 ? (
                    countriesToDisplay.map((country, countryIdx) => 
                        <CountryMiniCard isDarkMode={isDarkMode} country={country} key={countryIdx} />
                    )
                ) : (
                    countries.length > 0 && selectedRegion !== "Filter by Region (all)" && inputValue !== "" ? (
                        <div className={`h-screen text-center mt-20 xl:mt-20 xl:col-span-4 xl:text-3xl ${isDarkMode ? 'bg-gray-800 text-white' : `bg-lightMode`}`}>
                            No result match your search
                        </div>
                   ) : (
                        <div className={`h-screen text-center mt-20 xl:mt-20 xl:col-span-4 xl:text-3xl ${isDarkMode ? 'bg-gray-800 text-white' : `bg-lightMode`}`}>
                            Loading...                    
                       </div>
                   )
                )}
            </div>
        </div>
     );
}
 
export default HomePage;