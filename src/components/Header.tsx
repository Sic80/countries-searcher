import Switch from "react-switch";
import { IoMoonOutline, IoSunnyOutline } from 'react-icons/io5';
import { useDispatch, useSelector } from "react-redux";
import { toggleTheme } from "./reducers";
import { RootState } from "./types/RootState";
import { useHistory } from 'react-router-dom';
 
const Header: React.FunctionComponent = () => {

    const dispatch = useDispatch();
    const isDarkMode = useSelector((state: RootState) => state.isDarkMode);
    const history = useHistory();

    return ( 
        <header className={`h-20 xl:h-20 grid grid-cols-2 border-b-2 relative z-20 select-none ${isDarkMode ? "bg-myGray text-white border-gray-800" : "bg-white"} sticky top-0`}>
            <h1 
                className="grid content-center ml-5 xl:ml-10 font-bold xl:text-2xl cursor-pointer"
                onClick={() => history.push('/')}
            >
                Where in the world?
            </h1>
            <div className="flex justify-end  pr-10 xl:pr-20 items-center text-center select-none">
                <Switch
                    onChange={() => dispatch(toggleTheme())}
                    checked={isDarkMode}
                    offColor="#1f2937"
                    uncheckedIcon={<IoMoonOutline className="mt-1 ml-1.5 text-white"/>}	
                    onColor="#FFFFFF"
                    checkedIcon={<IoSunnyOutline className="text-black pt-1 ml-0.5" size={24} />}
                    onHandleColor="#1f2937"
                />

            </div>
        </header>
     );
}
 
export default Header;