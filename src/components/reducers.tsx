
interface toggleThemeAction {
    type: typeof TOGGLE_THEME
}
const TOGGLE_THEME = "TOGGLE_THEME";
export const toggleTheme = () => ({
    type: TOGGLE_THEME
});

export interface ThemeToggle {
    isDarkMode: boolean
}

export const themeToggle = (state: ThemeToggle ={isDarkMode: false}, action: toggleThemeAction) => {
    switch(action.type) {
        case TOGGLE_THEME: {
            return {isDarkMode: !state.isDarkMode};
        }
        default: {
            return state;
        }
    }
}