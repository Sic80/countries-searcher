import { IoIosArrowDown, IoIosArrowUp } from 'react-icons/io';
import { useState } from 'react';
import { useSelector } from 'react-redux';
import { RootState } from './types/RootState';


export interface DropDownProps {
    selectedRegion: string;
    setSelectedRegion: (e: any) => void;
}
 
const DropDown: React.FunctionComponent<DropDownProps> = ({ selectedRegion, setSelectedRegion }) => {

    const [isDropdownDown, setIsDropdownDown] = useState<boolean>(false);
    const isDarkMode = useSelector((state: RootState) => state.isDarkMode);
    const regions: string[] = ["Filter by Region (all)", "Africa", "Americas", "Asia", "Europe", "Oceania"]; 


    return ( 
        <div className="grid grid-cols-1 h-16 ml-4 mb-10 md:mt-5 md:justify-items-end md:pr-20">
                <div 
                    className={`h-14 xl:h-16 grid grid-cols-6 w-3/5 xl:w-2/5 rounded-lg shadow-md pl-4 ${isDarkMode ? "bg-gray-700 text-white": "bg-white" }`}
                
                >
                    <span className="col-span-5 flex items-center pl-2 font-semibold">{selectedRegion}</span>
                    <span 
                        className="flex items-center"
                        onClick={() => setIsDropdownDown(!isDropdownDown)}
                    >
                        {isDropdownDown ? <IoIosArrowUp size={25} className="cursor-pointer" /> : <IoIosArrowDown size={25} className="cursor-pointer"/>}
                    </span>
                </div> 
                {isDropdownDown &&
                    <div className={`z-10 relative ${isDarkMode ? 'bg-gray-700 text-white' : 'bg-white'} rounded-lg w-3/5 xl:w-2/5  pl-5 mt-1 pt-2 pb-2 font-semibold shadow-md`}>
                       {regions.map((region, regionIdx) =>
                            region !== selectedRegion &&
                                <p 
                                    key={regionIdx} 
                                    className="mb-2 cursor-pointer"
                                    onClick={() => {
                                        setSelectedRegion(region);
                                        setIsDropdownDown(!isDropdownDown)
                                    }}
                                >
                                    {region}
                                </p>         
                       )} 
                       
                    
                    </div>
                }
            </div>
     );
}
 
export default DropDown;