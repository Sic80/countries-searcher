export interface Name {
    common: string;
    official: string;
    nativeName: {
        common: string;
        official: string;
    }
}