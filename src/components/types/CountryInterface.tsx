import { Currency } from "./CurrencyInterface";
import { Language } from "./LanguageInterface";
import { Name } from "./NameInterface";
import { RegionalBloc } from "./RegionalBlocInterface";

export interface Country {
    alpha2Code: string,
    alpha3Code: string,
    altSpellings: string[],
    area: number,
    borders: string[],
    callingCodes: string[],
    capital: string,
    cioc: string,
    currencies: Currency[],
    demonym: string,
    flags: {svg: string, png: string},
    gini: number,
    languages: Language[],
    latlng: number[],
    name: string,
    nativeName: string,
    numericCode: string,
    population: number,
    region: string,
    regionalBlocs: RegionalBloc[],
    subRegion: string,
    timezones: string,
    topLevelDomain: string[],
    translations: {key: string}
}



/*


altSpellings: (3) ['BA', 'Bosnia-Herzegovina', 'Босна и Херцеговина']
area: 51209
borders: (3) ['HRV', 'MNE', 'SRB']
capital: ['Sarajevo']
cca2: "BA"
cca3: "BIH"
ccn3: "070"
cioc: "BIH"
currencies: {BAM: {…}}
demonyms: {eng: {…}, fra: {…}}
flag: "🇧🇦"
flags: (2) ['https://restcountries.com/data/bih.svg', 'https://restcountries.com/data/png/bih.png']
idd: {root: '+3', suffixes: Array(1)}
independent: true
landlocked: false
languages: {bos: 'Bosnian', hrv: 'Croatian', srp: 'Serbian'}
latlng: (2) [44, 18]
name: {common: 'Bosnia and Herzegovina', official: 'Bosnia and Herzegovina', nativeName: {…}}
region: "Europe"
status: "officially-assigned"
subregion: "Southern Europe"
tld: ['.ba']
translations: {ces: {…}, cym: {…}, deu: {…}, est: {…}, fin: {…}, …}
unMember: 

export interface Country {

    altSpellings: string[];
    area: number;
    borders: string[];
    capital: string[];
    cca2: string;
    cca3: string;
    ccn3: string;
    cioc: string;
    currencies: {key: Currency}; 
    demonyms: object;
    flag: string;
    flags: {svg: string; png: string};
    idd: object;
    independent: boolean;
    landlocked: boolean;
    languages: {key: string};
    latlng: number[];
    name: Name;
    region: string;
    status: string;
    subregion: string;
    tld: string[];
    translations: object;
    unMember: boolean;
}*/

