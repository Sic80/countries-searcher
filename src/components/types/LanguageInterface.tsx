export interface Language {
    iso639_1: string,
    iso639_2: string,
    name: string,
    nativeName: string
}


//{iso639_1: "ps", iso639_2: "pus", name: "Pashto", nativeName: "پښتو"}