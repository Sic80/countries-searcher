import { useEffect, useState } from 'react';
import { Country } from './types/CountryInterface';

export default function useCountries() {

    const [loaded, setLoaded] = useState(false);
    

    useEffect(() => {
        fetch('https://restcountries.com/v2/all')
        .then(response => response.json())
        .then(result => {
            const countries = result.map((country: Country) => {
                return country;    
            })
            localStorage.setItem("countries", JSON.stringify(countries));
            setLoaded(true);

        })
    }, [loaded]);

    const jsonCountries = localStorage.getItem("countries");
    let countries = jsonCountries !== null ? JSON.parse(jsonCountries) : [];
    return countries;
}