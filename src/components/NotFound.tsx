import Header from "./Header"
import { useSelector } from 'react-redux';
import { RootState } from './types/RootState';

const NotFound = () => {

    const isDarkMode = useSelector((state: RootState) => state.isDarkMode);


    return (
        <div className="">
            <Header />
            <div className={`grid ${isDarkMode ? 'bg-gray-800 text-white' : 'bg-lightMode'} h-screen `}>
                <p className=" text-center mt-20 font-bold text-5xl xl:text-9xl xl:mt-32 self-center">404</p>
                <p className="text-center xl:text-3xl">
                    We can't find the country you are looking for. <br />
                    Sorry for the inconvenience.
                </p> 

            </div>
        </div>
    )
}

export default NotFound