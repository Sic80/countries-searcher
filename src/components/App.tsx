import { Route, Switch } from 'react-router-dom';
import HomePage from './HomePage';
import CountryCard from './CountryCard';
import NotFound from './NotFound';
import { Country } from './types/CountryInterface';
import useCountries from './useCountries';

function App() {

  const countries: Country[] = useCountries();
  const countriesName = countries.map((country: Country) => country.name)
  const countryCostraint = countriesName.join("|").replaceAll(/(\(|\)|\s)/g  , "-");
  
  return (
    <div>
      <Switch>
        <Route exact path="/" render={(props) => <HomePage {...props}  countries={countries}/>} />
        <Route exact path={`/:country(${countryCostraint})`} component={CountryCard} />
        <Route component={NotFound} />
      </Switch>
    </div>
  );
}

export default App;
