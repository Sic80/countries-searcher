const defaultTheme = require('tailwindcss/defaultTheme')

module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    screens: {
      'xs': '400px',
      ...defaultTheme.screens,
    },
    extend: {
      colors: {
        lightMode: 'hsl(0, 0%, 98%)',
        myGray: "rgb(43, 55, 67)"
      },
      height: {
        120: "30rem",
        160: "34rem",
        170: "34.5rem"
      },
      maxHeight: {
        160: "34rem"
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
